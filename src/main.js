import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import VueLazyload from 'vue-lazyload'


Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: '/lazyload.jpg', 
  loading: '/lazyload.svg', 
  attempt: 1
})

Vue.config.productionTip = false


axios.defaults.baseURL = 'https://suitmedia-backend.suitdev.com/api'


Vue.prototype.$http = axios

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
