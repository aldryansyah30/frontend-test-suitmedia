import Vue from 'vue'
import VueRouter from 'vue-router'
import IdeasView from '../views/IdeasView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'ideas',
    component: IdeasView
  },
 
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
